
import io
import os
import os.path
import sys
import struct # for float reading
import math

# set this value to a maximum bound valid for your domain
vertexValueBoundMaximum = 1e5

warnForUnnormalisedNormals = False


def readHeader(binary_stream):

    tell = binary_stream.tell() # store current position

    binary_stream.seek(0)

    header = stlfile.read(80)   # read whole header

    binary_stream.seek(tell) # put stream pointer back

    return header


def readTriCount(binary_stream):

    tell = binary_stream.tell()

    binary_stream.seek(80) # seek past the header to the triangle count position

    triCount = int.from_bytes(binary_stream.read(4), byteorder='little', signed=False)

    binary_stream.seek(tell) # put stream back

    return triCount


def getLength(binary_stream):

    tell = binary_stream.tell()

    binary_stream.seek(0, os.SEEK_END)

    length = binary_stream.tell() # not actually sure this is accurate, theres a not in text io documentation

    binary_stream.seek(tell) # put back

    return length


# returns number of invalid entry, 0 otherwise
def checkVector(binary_stream, check_normal = False):

    maxValue = vertexValueBoundMaximum
    minValue = -vertexValueBoundMaximum

    vector = []

    vector.append(struct.unpack('f', binary_stream.read(4))[0])
    vector.append(struct.unpack('f', binary_stream.read(4))[0])
    vector.append(struct.unpack('f', binary_stream.read(4))[0])

    # check normalised
    if(check_normal == True):

        magnitude = math.sqrt((vector[0]*vector[0]) + (vector[1]*vector[1]) + (vector[2]*vector[2]));

        if(magnitude == 0.0):
            print("normal length 0")
            return 1

        if(warnForUnnormalisedNormals == True and (magnitude > 1.0000001 or magnitude < 0.9999998)):
            print("[Warning] Normal magnitude: ",magnitude)
            return -1

    for i in range(0, 3):

        # check reasonable bounds
        entry = vector[i]

        if(entry < minValue or entry > maxValue):
            print("vertex: ",vector[0],vector[1],vector[2])
            return i+1

    return 0


def printTriangle(binary_stream):

    vector = []

    for i in range(0,12):
        vector.append(struct.unpack('f', binary_stream.read(4))[0])

    attributes = int.from_bytes(stlfile.read(2), byteorder='little', signed=False)

    print("invalid triangle:")
    print("normal:",vector[0],vector[1],vector[2])
    print("vertex0:",vector[3],vector[4],vector[5])
    print("vertex1:",vector[6],vector[7],vector[8])
    print("vertex2:",vector[9],vector[10],vector[11])
    print("attributes:",attributes)


def checkTriangles(binary_stream, numTris):

    tell = binary_stream.tell()

    binary_stream.seek(84)

    for i in range(0, numTris):
        #print ("checking ", i)
        normOk = checkVector(binary_stream, True)
        vert0 = checkVector(binary_stream)
        vert1 = checkVector(binary_stream)
        vert2 = checkVector(binary_stream)

        attributes = int.from_bytes(stlfile.read(2), byteorder='little', signed=False)

        if(normOk != 0):
            print("[warning] non-normalized normal on triangle", i)
            normOk = 0

        if(vert0 != 0):
            print("[warning] value out of reasonable bounds, vertex 0, triangle", i)

        if(vert1 != 0):
            print("[warning] value out of reasonable bounds, vertex 1, triangle", i)

        if(vert2 != 0):
            print("[warning] value out of reasonable bounds, vertex 2, triangle", i)

        if(attributes != 0):
            print("[error] attribute value invalid",attributes, "triangle",i)

        if(normOk + vert0 + vert1 + vert2 + attributes > 0):
            binary_stream.seek(-50, os.SEEK_CUR)
            printTriangle(binary_stream)
            sys.exit()


    binary_stream.seek(tell) # put back


def getFileToOpen():

    # get stl file
    if(len(sys.argv) > 2):

        firstArg = sys.argv[1]
        if(firstArg == "-w"):
            warnForUnnormalisedNormals = True
            fileToOpen = sys.argv[2]
        elif(sys.argv[1] == "-w"):
            warnForUnnormalisedNormals = True
            fileToOpen = sys.argv[1]
        else:
            print("Invalid parameters, expecting [path]. Use -w to warn on non 1 normal magnitudes.")
    else:
        fileToOpen = input("enter filepath to open:")
        fileToOpen = fileToOpen.replace('"','')

        if(fileToOpen.endswith(".stl") == False):
            print("\n[Warning] File extension isn't STL\n")

    if(os.path.isfile(fileToOpen) == False):
        print("[Error] Unable to find specified file",fileToOpen)
        sys.exit() 

    return fileToOpen


# start

fileToOpen = getFileToOpen()

with open(fileToOpen, "rb") as stlfile :

    print("[Info] Opened", fileToOpen)

    length = getLength(stlfile)

    # triangle count tests
    containedNumTris = (length - 84) / 50

    readTriCount = readTriCount(stlfile)

    if(containedNumTris != readTriCount):
        print("[Error] File triangle count", readTriCount, "doesn't match actual value", containedNumTris)
        sys.exit()

    print("[Info] Valid triangle count:", readTriCount)
    
    # header tests
    expectedHeader = bytes([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

    header = readHeader(stlfile)

    if(header == expectedHeader):
        print("[Info] Header matches expected")
    else:
        print("[Error] Unexpected header", header)
        sys.exit()

    # triangle tests
    checkTriangles(stlfile, min(readTriCount, containedNumTris))

    print("[Info] All triangles in valid format and inside bounds")
    print("[Info] Done")
