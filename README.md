# STLBinaryLint

This is a small python script that can find issues with STL files in binary format.

Can determine:
Invalid headers
Incorrect triangle count
